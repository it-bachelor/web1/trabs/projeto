import { mockObjArray } from '../mock.js';

const getUrlParams = () => {
    const params = new URLSearchParams(window.location.search);
    return {
        member1: parseInt(params.get('member1'), 10),
        member2: parseInt(params.get('member2'), 10),
    };
}

const getMemberById = (id) => {
  return mockObjArray.find(member => member.id === id);
}

const getDayName = (date) => {
    const days = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'];
    return days[date.getUTCDay()];
}

const formatUtcTimeInLocalTimezone = (time, timezone) => {
    return new Intl.DateTimeFormat('pt-BR', {
        hour: '2-digit',
        minute: '2-digit',
        hour12: false,
        timeZone: timezone,
    }).format(time);
}

const formatDateInTimezone = (time, timezone) => {
    return new Date(time.toLocaleString('en-US', { timeZone: timezone }));
}

function buildAvailabilityTable(member1, member2) {
    const tableBody = document.querySelector('#availability-table tbody');
    const header1 = document.getElementById('member1-header');
    const header2 = document.getElementById('member2-header');

    header1.innerText = `${member1.name} (${member1.timezone})`;
    header2.innerText = `${member2.name} (${member2.timezone})`;

    const now = new Date();
    now.setUTCHours(0, 0, 0, 0);

    for (let hour = 0; hour < 24; hour++) {
        const utcTime = new Date(now.getTime() + hour * 3600 * 1000);
        const row = document.createElement('tr');
        const utcCell = document.createElement('td');
        utcCell.innerText = `${getDayName(utcTime)} ${utcTime.toISOString().substring(11, 16)}`;
        row.appendChild(utcCell);

        const member1Cell = document.createElement('td');
        const member1Time = formatDateInTimezone(utcTime, member1.timezone);
        member1Cell.innerText = `${getDayName(member1Time)} ${formatUtcTimeInLocalTimezone(utcTime, member1.timezone)}`;
        member1Cell.className = isWorking(member1, member1Time) ? 'trabalhando' : 'folga';
        row.appendChild(member1Cell);

        const member2Cell = document.createElement('td');
        const member2Time = formatDateInTimezone(utcTime, member2.timezone);
        member2Cell.innerText = `${getDayName(member2Time)} ${formatUtcTimeInLocalTimezone(utcTime, member2.timezone)}`;
        member2Cell.className = isWorking(member2, member2Time) ? 'trabalhando' : 'folga';
        row.appendChild(member2Cell);

        tableBody.appendChild(row);
    }
}

function isWorking(member, time) {
    const day = time.getDay();
    const workHours = member.workHours[day] || [];
    const timeStr = time.toTimeString().substring(0, 8);

    return workHours.some(period => timeStr >= period.start && timeStr < period.end);
}

window.addEventListener('load', () => {
    const { member1, member2 } = getUrlParams();
    const member1Mock = getMemberById(member1);
    const member2Mock = getMemberById(member2);

    if (member1Mock && member2Mock) {
        buildAvailabilityTable(member1Mock, member2Mock);
    } else {
        const tableBody = document.querySelector('#availability-table');
        tableBody.remove();
        
        const pageBody = document.getElementById('schedule-availability');
        const errorMsg = document.createElement('strong');
        errorMsg.innerText = 'Membros não selecionados ou só 1 selecionado... =(';
        pageBody.appendChild(errorMsg);

        const sugestao = document.createElement('p');
        sugestao.innerText = 'Tente selecionar 2 membros na página de "Listar Membros"';
        pageBody.appendChild(sugestao);
    }
});
