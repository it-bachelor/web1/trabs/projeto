import { mockObjArray } from '../mock.js';



document.addEventListener('DOMContentLoaded', function() {
    const selectedMemberIds = [];

    mockObjArray.forEach(member => {
        const memberElement = document.createElement('div');
        memberElement.classList.add('member');

        const photoElement = document.createElement('img');
        photoElement.src = member.photo;
        memberElement.appendChild(photoElement);
        
        const nameElement = document.createElement('p');
        nameElement.textContent = member.name;
        memberElement.appendChild(nameElement);

        const zoneElement = document.createElement('p');
        zoneElement.textContent = member.timezone;
        memberElement.appendChild(zoneElement);

        const roleElement = document.createElement('p');
        roleElement.textContent = member.role;
        memberElement.appendChild(roleElement);
   
        const dayElement = document.createElement('p');
        if (typeof member.workHours === 'object') {
            const currentDay = new Date().getDay();
            const workHoursToday = member.workHours[currentDay];
            if (workHoursToday && workHoursToday.length > 0) {
                dayElement.textContent = workHoursToday.map(
                    period => `${period.start} - ${period.end}`
                ).join(', ');
            } else {
                dayElement.textContent = 'No work hours today';
            }
        } else {
            dayElement.textContent = 'Work hours not available';
        }
        memberElement.appendChild(dayElement);

        const checkboxElement = document.createElement('input');
        checkboxElement.type = 'checkbox';
        checkboxElement.name = 'selected_members';
        
        checkboxElement.addEventListener('change', function() {
            if (checkboxElement.checked) {
                if (selectedMemberIds.length < 2) {
                    selectedMemberIds.push(member.id);
                } else {
                    checkboxElement.checked = false;
                }
            } else {
                const index = selectedMemberIds.findIndex(id => id === member.id);
                if (index > -1) {
                    selectedMemberIds.splice(index, 1);
                }
            }
            document.getElementById('compareButton').disabled = selectedMemberIds.length !== 2;
        });

        memberElement.appendChild(checkboxElement);

        document.getElementById('teamList').appendChild(memberElement);
    });

    document.getElementById('compareButton').addEventListener('click', function() {
        if (selectedMemberIds.length === 2) {
            const params = new URLSearchParams();
            params.append('member1', selectedMemberIds[0]);
            params.append('member2', selectedMemberIds[1]);
            window.location.href = `disponibilidade.html?${params.toString()}`;
        }
    });
});