const getMenu = (enabledIndex) => {
  const menuItems = [
    { nome: "Tela Inicial", link: "./index.html", current: false },
    // { nome: "Cadastro Membros", link: "./cadastro_membros.html", current: false },
    { nome: "Ver Horários Atuais", link: "./relogios.html", current: false },
    { nome: "Listar Membros", link: "./listar-membros.html", current: false },
    { nome: "Verificar Disponibilidade", link: "./disponibilidade.html", current: false },
    // { nome: "Converter Horários", link: "./conversor.html", current: false },
  ];
  menuItems[enabledIndex].current = true;
  return menuItems;
}

function buildMenu(index) {
  const menuDiv = document.getElementById("menu");
  const ul = document.createElement("ul");
  const menu = getMenu(index);
  menu.forEach((item) => {
      const li = document.createElement("li");
      if (item.current) {
        li.setAttribute("class", "current");
      }
      const a = document.createElement("a");
      a.textContent = item.nome;
      a.href = item.link;
      li.appendChild(a);
      ul.appendChild(li);
  });
  menuDiv.appendChild(ul);
}
