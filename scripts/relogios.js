import { mockObjArray } from '../mock.js';

const timezones = mockObjArray.map((member) => {
    return member.timezone;
}).sort();

document.addEventListener('DOMContentLoaded', function() {
    
  const timezonesList = document.getElementById('timezonesList');

  function getLocalTime(timezone) {
      const date = new Date();
      const options = { timeZone: timezone };
      return date.toLocaleTimeString('pt-BR', options);
  }

  function createLocalTimes() {
    timezones.forEach(function(timezone) {
        const localTime = getLocalTime(timezone);
        const timezoneDiv = document.createElement('div');
        timezoneDiv.classList.add('timezone');
        timezoneDiv.id = timezone;
        timezoneDiv.textContent = `${timezone}: ${localTime}`;
        timezonesList.appendChild(timezoneDiv);
    });
  }

    createLocalTimes();

    function updateLocalTimes() {
        timezones.forEach(function(timezone) {
            const localTime = getLocalTime(timezone);
            const timezoneDiv = document.getElementById(timezone);
            timezoneDiv.textContent = `${timezone}: ${localTime}`;
        });
   }

    setInterval(updateLocalTimes, 1000);
});
