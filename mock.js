export const mockObjArray = [
  {
    id: 1,
    name: 'Ana',
    photo: 'https://cdn2.thecatapi.com/images/3hb.jpg',
    timezone: 'Africa/Johannesburg',
    role: 'DevOps',
    workHours: {
      0: [],
      1: [
        { start: '08:00:00', end: '12:00:00' },
        { start: '14:00:00', end: '18:00:00' },
      ],
      2: [
        { start: '08:00:00', end: '12:00:00' },
        { start: '14:00:00', end: '18:00:00' },
      ],
      3: [
        { start: '08:00:00', end: '12:00:00' },
        { start: '14:00:00', end: '18:00:00' },
      ],
      4: [
        { start: '08:00:00', end: '12:00:00' },
        { start: '14:00:00', end: '18:00:00' },
      ],
      5: [
        { start: '08:00:00', end: '12:00:00' },
        { start: '14:00:00', end: '18:00:00' },
      ],
      6: [
        { start: '08:00:00', end: '12:00:00' },
        { start: '14:00:00', end: '18:00:00' },
      ],
    },
  },
  {
    id: 2,
    name: 'Júlia',
    photo: 'https://cdn2.thecatapi.com/images/xplMlG1PT.jpg',
    timezone: 'Europe/Rome',
    role: 'CTO',
    workHours: {
      0: [
        { start: '07:00:00', end: '12:00:00' },
        { start: '15:00:00', end: '18:00:00' },
      ],
      1: [],
      2: [
        { start: '07:00:00', end: '12:00:00' },
        { start: '15:00:00', end: '18:00:00' },
      ],
      3: [
        { start: '07:00:00', end: '12:00:00' },
        { start: '15:00:00', end: '18:00:00' },
      ],
      4: [
        { start: '08:00:00', end: '10:00:00' },
        { start: '12:00:00', end: '14:00:00' },
        { start: '16:00:00', end: '18:00:00' },
      ],
      5: [],
      6: [
        { start: '07:00:00', end: '12:00:00' },
        { start: '15:00:00', end: '18:00:00' },
      ],
    },
  },
  {
    id: 3,
    name: 'João',
    photo: 'https://cdn2.thecatapi.com/images/UCifm-g71.jpg',
    timezone: 'Asia/Dubai',
    role: 'Data Eng.',
    workHours: {
      0: [
        { start: '09:00:00', end: '12:00:00' },
        { start: '13:00:00', end: '18:00:00' },
      ],
      1: [
        { start: '09:00:00', end: '12:00:00' },
        { start: '13:00:00', end: '18:00:00' },
      ],
      2: [],
      3: [
        { start: '09:00:00', end: '12:00:00' },
        { start: '13:00:00', end: '18:00:00' },
      ],
      4: [],
      5: [
        { start: '09:00:00', end: '12:00:00' },
        { start: '13:00:00', end: '18:00:00' },
      ],
      6: [
        { start: '09:00:00', end: '12:00:00' },
        { start: '13:00:00', end: '18:00:00' },
      ],
    },
  },
  {
    id: 4,
    name: 'Mariana',
    photo: 'https://cdn2.thecatapi.com/images/vg.jpg',
    timezone: 'America/Los_Angeles',
    role: 'AI Eng.',
    workHours: {
      0: [],
      1: [],
      2: [
        { start: '08:00:00', end: '12:00:00' },
        { start: '14:00:00', end: '16:00:00' },
        { start: '18:00:00', end: '20:00:00' },
      ],
      3: [
        { start: '08:00:00', end: '12:00:00' },
        { start: '14:00:00', end: '16:00:00' },
        { start: '18:00:00', end: '20:00:00' },
      ],
      4: [
        { start: '08:00:00', end: '12:00:00' },
        { start: '14:00:00', end: '16:00:00' },
        { start: '18:00:00', end: '20:00:00' },
      ],
      5: [
        { start: '08:00:00', end: '12:00:00' },
        { start: '14:00:00', end: '16:00:00' },
        { start: '18:00:00', end: '20:00:00' },
      ],
      6: [
        { start: '08:00:00', end: '12:00:00' },
        { start: '14:00:00', end: '16:00:00' },
        { start: '18:00:00', end: '20:00:00' },
      ],
    },
  },
  {
    id: 5,
    name: 'Lucas',
    photo: 'https://cdn2.thecatapi.com/images/b0p.png',
    timezone: 'Asia/Hong_Kong',
    role: 'PO',
    workHours: {
      0: [
        { start: '10:00:00', end: '12:00:00' },
        { start: '14:00:00', end: '18:00:00' },
        { start: '20:00:00', end: '22:00:00' },
      ],
      1: [],
      2: [],
      3: [
        { start: '10:00:00', end: '12:00:00' },
        { start: '14:00:00', end: '18:00:00' },
        { start: '20:00:00', end: '22:00:00' },
      ],
      4: [
        { start: '10:00:00', end: '12:00:00' },
        { start: '14:00:00', end: '18:00:00' },
        { start: '20:00:00', end: '22:00:00' },
      ],
      5: [
        { start: '10:00:00', end: '12:00:00' },
        { start: '14:00:00', end: '18:00:00' },
        { start: '20:00:00', end: '22:00:00' },
      ],
      6: [
        { start: '10:00:00', end: '12:00:00' },
        { start: '14:00:00', end: '18:00:00' },
        { start: '20:00:00', end: '22:00:00' },
      ],
    },
  },
  {
    id: 6,
    name: 'Beatriz',
    photo: 'https://cdn2.thecatapi.com/images/9vt.jpg',
    timezone: 'Atlantic/Madeira',
    role: 'UX Dgn.',
    workHours: {
      0: [],
      1: [
        { start: '08:00:00', end: '12:00:00' },
        { start: '14:00:00', end: '18:00:00' },
      ],
      2: [
        { start: '08:00:00', end: '12:00:00' },
        { start: '14:00:00', end: '18:00:00' },
      ],
      3: [
        { start: '08:00:00', end: '12:00:00' },
        { start: '14:00:00', end: '18:00:00' },
      ],
      4: [
        { start: '08:00:00', end: '12:00:00' },
        { start: '14:00:00', end: '18:00:00' },
      ],
      5: [
        { start: '08:00:00', end: '12:00:00' },
        { start: '14:00:00', end: '18:00:00' },
      ],
      6: [],
    },
  },
  {
    id: 7,
    name: 'Pedro',
    photo: 'https://cdn2.thecatapi.com/images/TuSyTkt2n.jpg',
    timezone: 'Asia/Singapore',
    role: 'SWE',
    workHours: {
      0: [
        { start: '08:00:00', end: '11:00:00' },
        { start: '13:00:00', end: '15:00:00' },
        { start: '17:00:00', end: '20:00:00' },
      ],
      1: [
        { start: '08:00:00', end: '11:00:00' },
        { start: '13:00:00', end: '15:00:00' },
        { start: '17:00:00', end: '20:00:00' },
      ],
      2: [],
      3: [],
      4: [
        { start: '08:00:00', end: '11:00:00' },
        { start: '13:00:00', end: '15:00:00' },
        { start: '17:00:00', end: '20:00:00' },
      ],
      5: [
        { start: '08:00:00', end: '11:00:00' },
        { start: '13:00:00', end: '15:00:00' },
        { start: '17:00:00', end: '20:00:00' },
      ],
      6: [
        { start: '08:00:00', end: '11:00:00' },
        { start: '13:00:00', end: '15:00:00' },
        { start: '17:00:00', end: '20:00:00' },
      ],
    },
  },
  {
    id: 8,
    name: 'Camila',
    photo: 'https://cdn2.thecatapi.com/images/MTUzMTY1NQ.jpg',
    timezone: 'Australia/Sydney',
    role: 'QA',
    workHours: {
      0: [
        { start: '08:00:00', end: '10:00:00' },
        { start: '12:00:00', end: '14:00:00' },
        { start: '16:00:00', end: '20:00:00' },
      ],
      1: [
        { start: '08:00:00', end: '10:00:00' },
        { start: '12:00:00', end: '14:00:00' },
        { start: '16:00:00', end: '20:00:00' },
      ],
      2: [
        { start: '08:00:00', end: '10:00:00' },
        { start: '12:00:00', end: '14:00:00' },
        { start: '16:00:00', end: '20:00:00' },
      ],
      3: [],
      4: [],
      5: [
        { start: '08:00:00', end: '10:00:00' },
        { start: '12:00:00', end: '14:00:00' },
        { start: '16:00:00', end: '20:00:00' },
      ],
      6: [
        { start: '08:00:00', end: '10:00:00' },
        { start: '12:00:00', end: '14:00:00' },
        { start: '16:00:00', end: '20:00:00' },
      ],
    },
  },
  {
    id: 9,
    name: 'Rafael',
    photo: 'https://cdn2.thecatapi.com/images/9fn.jpg',
    timezone: 'America/Noronha',
    role: 'Arch',
    workHours: {
      0: [
        { start: '07:00:00', end: '08:00:00' },
        { start: '10:00:00', end: '13:00:00' },
        { start: '15:00:00', end: '19:00:00' },
      ],
      1: [
        { start: '07:00:00', end: '08:00:00' },
        { start: '10:00:00', end: '13:00:00' },
        { start: '15:00:00', end: '19:00:00' },
      ],
      2: [
        { start: '07:00:00', end: '08:00:00' },
        { start: '10:00:00', end: '13:00:00' },
        { start: '15:00:00', end: '19:00:00' },
      ],
      3: [
        { start: '07:00:00', end: '08:00:00' },
        { start: '10:00:00', end: '13:00:00' },
        { start: '15:00:00', end: '19:00:00' },
      ],
      4: [],
      5: [],
      6: [
        { start: '07:00:00', end: '08:00:00' },
        { start: '10:00:00', end: '13:00:00' },
        { start: '15:00:00', end: '19:00:00' },
      ],
    },
  },
  {
    id: 10,
    name: 'Gabriel',
    photo: 'https://cdn2.thecatapi.com/images/1p1.jpg',
    timezone: 'Pacific/Honolulu',
    role: 'BA',
    workHours: {
      0: [
        { start: '08:00:00', end: '12:00:00' },
        { start: '16:00:00', end: '20:00:00' },
      ],
      1: [
        { start: '08:00:00', end: '12:00:00' },
        { start: '16:00:00', end: '20:00:00' },
      ],
      2: [
        { start: '08:00:00', end: '12:00:00' },
        { start: '16:00:00', end: '20:00:00' },
      ],
      3: [
        { start: '08:00:00', end: '12:00:00' },
        { start: '16:00:00', end: '20:00:00' },
      ],
      4: [
        { start: '08:00:00', end: '12:00:00' },
        { start: '16:00:00', end: '20:00:00' },
      ],
      5: [],
      6: [],
    },
  },
];
