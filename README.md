# executando

- com node instalado, instalar `http-server`:

```bash
npm install -g http-server
```

executar o servidor:
  
```bash
http-server --cors
```

acessar no browser: `http://127.0.0.1:8080`
